#!/usr/bin/env python3

"""
Fetches wiki markup from theiphonewiki, to then compare them with the output of
the scripts.
"""

import requests
import os

page_list = []

page_list.append(f"Firmware/iPod_touch/15.x")
page_list.append(f"Firmware/Apple_Vision/1.x")
page_list.append(f"Beta_Firmware/Apple_Vision/1.x")

for v in [11,12,13,14]:
    page_list.append(f"Firmware/Mac/{v}.x")
    page_list.append(f"Beta_Firmware/Mac/{v}.x")

page_list.append(f"Beta_Firmware/iPod_touch/15.x")
page_list.append(f"Beta_Firmware/Apple_Watch/9.x")
page_list.append(f"Beta_Firmware/Apple_Watch/10.x")
page_list.append(f"Firmware/Apple_Watch/9.x")
page_list.append(f"Firmware/Apple_Watch/10.x")
for device in ["iPhone", "iPad", "iPad_Pro", "iPad_Air", "iPad_mini", "HomePod", "Apple_TV"]:
    page_list.append(f"Beta_Firmware/{device}/15.x")
    page_list.append(f"Beta_Firmware/{device}/16.x")
    page_list.append(f"Beta_Firmware/{device}/17.x")
    page_list.append(f"Firmware/{device}/15.x")
    page_list.append(f"Firmware/{device}/16.x")
    page_list.append(f"Firmware/{device}/17.x")

sess = requests.session()

for page in page_list:
    print(f"Getting {page}")
    filename = page.replace("/", "_") + ".wiki"

    resp = sess.get("https://theapplewiki.com/index.php", params={"action":"raw", "title": page}, stream=True)
    assert resp.status_code == 200
    with open(os.path.join("wiki_pages", filename), "wb") as fd:
        for chunk in resp.iter_content(4096):
            fd.write(chunk)
