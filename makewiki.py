#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Nicolás Alvarez <nicolas.alvarez@gmail.com>
#
# SPDX-License-Identifier: MIT

import os
import sys
import json
import yaml
import base64
import requests
import plistlib
import re

from jinja2 import Environment, FileSystemLoader, select_autoescape

rsess = requests.session()

class MyEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, bytes):
            return base64.b64encode(o).decode('ascii')
        return json.JSONEncoder.default(self, o)

def get_manifest(ipsw_url):
    base_url, _, ipsw_fn = ipsw_url.rpartition('/')

    cache_fn = "manifest_cache/" + ipsw_fn + ".json"

    if os.path.isfile(cache_fn):
        with open(cache_fn, "rb") as cache_f:
            return json.load(cache_f)
    else:
        if not base_url.startswith("https://updates.cdn-apple.com/"): return None

        manifest_url = base_url + "/BuildManifest.plist"
        print(f"downloading from {manifest_url}", file=sys.stderr)
        response = rsess.get(manifest_url)
        assert response.status_code == 200
        data = plistlib.loads(response.content)
        json_data = json.dumps(data, cls=MyEncoder)

        with open(cache_fn, "w") as cache_f:
            cache_f.write(json_data)

        return data

def get_preferred_source(info, models, fw_type='ipsw'):
    models = set(models)
    for source in info['sources']:
        deviceMap = set(source['deviceMap'])
        if source['type'] == fw_type and deviceMap.issuperset(models):
            # all requested models are in this deviceMap
            selected_source = source
            break
    else:
        return (None, None, None)

    def link_score(link):
        if link['url'].startswith('http://'):
            is_https = 0
        elif link['url'].startswith('https://'):
            is_https = 1
        else:
            raise RuntimeError(f"URL {link['url']!r} is not http or https")

        is_direct_dl = 1
        if link['url'].startswith('https://developer.apple.com/services-account/download'):
            is_direct_dl = 0

        if 'catalog' not in link:
            catalog = 0
        elif link['catalog'] == 'public-beta':
            catalog = 1
        elif link['catalog'] == 'dev-beta':
            catalog = 2
        else:
            raise RuntimeError(f"Catalog {link['catalog']!r} unrecognized")

        return (is_direct_dl, is_https, catalog)

    pref_link = max(selected_source['links'], key=link_score)
    assert pref_link

    ipsw_url = pref_link['url']
    return (selected_source, ipsw_url, pref_link['active'])

class SpannedField:
    def __init__(self, value=""):
        self.value = value
        # If >1, this field has the same value in {rowspan} consecutive rows (including this one),
        # so we should output a rowspan= attribute on it.
        self.rowspan = 1
        # If true, this field has the same value in rows above, so we shouldn't output it,
        # it's already handled by the rowspan of the first one
        self.elided = False

    @classmethod
    def merge_cells(cls, fields):
        if len(fields) < 2: return

        first = fields[0]
        for field in fields[1:]:
            if field.value == first.value:
                first.rowspan += 1
                field.elided = True
            else:
                first = field

    def __str__(self):
        return self.value

    def __repr__(self):
        return f"<SpannedField value={repr(self.value)} rowspan={self.rowspan} elided={repr(self.elided)}>"

    def format_cell(self):
        if self.elided:
            return ""
        elif self.rowspan == 1:
            return f'| {self.value}'
        else:
            return f'| rowspan="{self.rowspan}" | {self.value}'

def load_data(basedir):
    filenames = [filename for filename in os.listdir(basedir) if filename.endswith(".json")]
    json_data = [json.load(open(os.path.join(basedir, filename), "r")) for filename in filenames]

    # expand "duplicate entries"
    for firmware in json_data.copy():
        if 'createDuplicateEntries' in firmware:
            for dup in firmware['createDuplicateEntries']:
                dup_firmware = firmware.copy()
                dup_firmware.update(dup)
                del dup_firmware['createDuplicateEntries']
                json_data.append(dup_firmware)

    return json_data

def load_devices():
    basedir = "appledb/deviceFiles"
    devices = {}
    for root, dirs, files in os.walk(basedir):
        for name in files:
            json_path = os.path.join(root, name)
            json_data = json.load(open(json_path, "r"))

            if 'identifier' in json_data:
                if isinstance(json_data['identifier'], list):
                    for ident in json_data['identifier']:
                        devices[ident] = json_data
                else:
                    devices[json_data['identifier']] = json_data

    return devices

DEVICES = load_devices()

def load_ota_manifests():
    print(f"Loading OTA manifests...", file=sys.stderr)
    ota_manifests = {}
    for filename in os.listdir('ota-manifests'):
        with open(f'ota-manifests/{filename}', 'rb') as plist_f:
            manifest = plistlib.load(plist_f)
        build_num = manifest['ProductBuildVersion']
        for product_type in manifest['SupportedProductTypes']:
            ota_manifests[(build_num, product_type)] = manifest
    return ota_manifests

g_ota_manifests = None

def get_ota_manifest(build_num, product_type):
    global g_ota_manifests
    if g_ota_manifests is None:
        g_ota_manifests = load_ota_manifests()
    return g_ota_manifests.get((build_num, product_type))

def deviceclass_from_id(device_id):
    if device_id not in DEVICES:
        raise KeyError(f"Couldn't find device ID {device_id}")
    if "board" not in DEVICES[device_id]:
        raise KeyError(f"Couldn't find board for device ID {device_id}")

    board = DEVICES[device_id]["board"]
    if isinstance(board, list):
        return board[0]
    else:
        return board

def get_basebands_by_class(manifest, device_classes):
    basebands = set()
    device_classes = [dc.upper() for dc in device_classes]
    for identity in manifest['BuildIdentities']:
        if identity['Info']['DeviceClass'].upper() in device_classes:
            if 'BasebandFirmware' in identity['Manifest']:
                basebands.add(identity['Manifest']['BasebandFirmware']['Info']['Path'])

    return basebands

def get_basebands_by_id(manifest, device_ids):
    return get_basebands_by_class(manifest, map(deviceclass_from_id, device_ids))

def baseband_version_from_path(path):
    if m := re.match('Firmware/[^-]+-([0-9.-]+)\.Release\.bbfw$', path):
        return m.group(1)
    else:
        raise RuntimeError(f"Couldn't get baseband version out of {path!r}")

def parse_build(build_num):
    m = re.fullmatch('([0-9]+)([A-Z])([0-9]+)([a-z])?', build_num)
    assert m
    major = int(m.group(1))
    minor = m.group(2)
    patch = int(m.group(3))
    # deal with the 'HW' branch
    if 2000 < patch < 2999:
        patch -= 2000
    if 8000 < patch < 8999:
        patch -= 8000
    return (major, minor, patch, m.group(4) or '0')

def get_firmwares(models, path, isBeta=False, fw_type='ipsw'):
    firmwares = load_data("appledb/osFiles/"+path)

    for firmware in firmwares:
        pi_field = firmware.get('preinstalled', None)
        if pi_field is None:
            is_preinstalled = False
        elif isinstance(pi_field, bool):
            is_preinstalled = pi_field
        elif isinstance(pi_field, list):
            is_preinstalled = models[0] in pi_field
        else:
            raise AssertionError()
        firmware['_preinstalled'] = is_preinstalled

    firmwares = [fw for fw in firmwares if (not fw.get('internal',False) and (fw.get('beta',False) or fw.get('rc',False)) == isBeta)]

    if isBeta:
        firmwares.sort(key=lambda fw: fw['released'])
    else:
        firmwares.sort(key=lambda fw: parse_build(fw['build']))

    infos = []

    orig_models = models
    for info in firmwares:
        # HACK: iOS 16.4 wasn't released for iPhone11,4, only for iPhone11,6.
        # But we process both ProductTypes together.
        # *Only* for this particular build, pretend we're not looking for iPhone11,4.
        models = orig_models
        if info['build'] == '20E247' and 'iPhone11,4' in models:
            models = [m for m in models if m != 'iPhone11,4']

        # Similar hack for the strange HomePod model
        if 'AudioAccessory1,2' in models and 'AudioAccessory1,2' not in info.get('deviceMap', []):
            models = [m for m in models if m != 'AudioAccessory1,2']

        # This is a weird AppleDB thing
        if info.get('uniqueBuild') == "21A559-bridge": continue

        if not all(model in info.get('deviceMap', []) for model in models): continue

        train = info.get('buildTrain')
        source = None
        ipsw_url = None
        ipsw_fn = None
        ipsw_active = None
        manifest = None
        baseband = '?'
        basebands = {}

        info = info.copy()

        info['models'] = models

        if 'sources' in info:
            source, ipsw_url, ipsw_active = get_preferred_source(info, models, fw_type)
            if ipsw_url:
                ipsw_fn = ipsw_url.rpartition('/')[2]

        if fw_type == 'ipsw' and ipsw_url:
            manifest = get_manifest(ipsw_url)
        if fw_type == 'ipsw' and not manifest:
            # no BuildManifest available, maybe because there is no ipsw URL at all, or maybe it failed to download;
            # maybe we have an OTA manifest saved?
            manifest = get_ota_manifest(info['build'], models[0])

        if manifest:
            train = manifest['BuildIdentities'][0]['Info']['BuildTrain']
            if not any(m.startswith(prefix) for m in models for prefix in ['Mac','AppleTV']):
                bbfws = get_basebands_by_id(manifest, models)
                if len(bbfws) == 1:
                    baseband = baseband_version_from_path(list(bbfws)[0])
                elif len(bbfws) > 1:
                    for b in bbfws:
                        m = re.match('Firmware/([A-Za-z0-9]+)-', b)
                        basebands[m.group(1)] = baseband_version_from_path(b)

        if not train and info['build'] in buildtrains:
            train = buildtrains[info['build']]

        # Get watchOS basebands from separate yaml.
        if baseband == '?' and basebands == {} and (models[0].startswith("Watch") or models[0].startswith("iPhone")):
            baseband = g_basebands.get(info['build'], '?')
            if baseband == '?':
                print(f"WARNING: unknown baseband for {info['version']} {info['build']}")

        info['applicable_source'] = source
        info['ipsw_url'] = ipsw_url
        info['ipsw_fn'] = ipsw_fn
        info['ipsw_active'] = ipsw_active
        if not train: print(f"WARNING: unknown buildtrain for {info['version']} {info['build']}", file=sys.stderr)
        info['train'] = train or "unknown"
        info['baseband'] = baseband
        info['basebands'] = basebands
        if 'iosVersion' not in info:
            info['iosVersion'] = None

        if 'ipd' not in info:
            pass
        elif len(info['ipd']) == 1:
            info['device_ipd'] = list(info['ipd'].values())[0]
        elif len(info['ipd']) > 1:
            for k,v in info['ipd'].items():
                if all(m.startswith(k) for m in models):
                    info['device_ipd'] = v
                    break
        if 'device_ipd' in info:
            info['ipd_filename'] = info['device_ipd'].rpartition('/')[2]

        # Time for some hacks.
        # Build 21F2081 added M2 support (Mac14,2 and Mac14,7),
        # technically it also supports M1 Macs
        # but we don't want to list it on M1 tables, so we remove it
        if path == "macOS/21x - 12.x":
            if info['build'] == '21F2081' and models[0] not in ('Mac14,2', 'Mac14,7'):
                continue

        # Apple officially called this "macOS 13.0 beta 3 Update" in the developer releases page,
        # and we prefer to use that naming in the wiki, even though appledb says "13.0 beta 3".
        if info['build'] == '22A5295i':
            info['version'] = "13.0 beta 3 Update"
        # ditto for iOS
        if info['build'] == '20A5312j':
            info['version'] = "16.0 beta 3 Update"

        # Disagreement between AppleDB and TheAppleWiki about what RC numbers to use
        if info['osStr'] == 'macOS' and 'RC' in info['version']:
            if info['build'] == '22G423': info['version'] = '13.6.3 RC 2'
            if info['build'] == '22G430': info['version'] = '13.6.3 RC 3'
            if info['build'] == '22G433': info['version'] = '13.6.3 RC 4'
            if info['build'] == '22G436': info['version'] = '13.6.3 RC 5'

        # Add a footnote to macOS 11.6
        if info['build'] == '20G165':
            info['date_note'] = '<ref name="ipsw116"/>'

        # Override date for iOS 16.0.1; it was released close to midnight
        # and AppleDB and The Apple Wiki disagree on the timezone to use,
        # so we have different dates.
        if info['build'] == '20A371':
            assert info['released'] == '2022-09-14'
            info['released'] = '2022-09-15'

        # This version had multiple ipd links
        if info['build'] == '20A371':
            info['doclinks'] = (
                "[https://updates.cdn-apple.com/2022FallFCS/documentation/012-70401/F11F048F-42E3-4126-AA96-4FDEF367610D/iPhoneiOSShortRTF.ipd iPhoneiOSShortRTF.ipd (version 1)]<br/>" +
                f"[{info['device_ipd']} iPhoneiOSShortRTF.ipd (version 2)]<br/>" +
                f"[{info['releaseNotes']} Release Notes]"
            )

        # Override date for iOS 17.0.1 for iPhone 15*
        # we don't know when/if it was actually released,
        # see https://theapplewiki.com/index.php?title=Firmware/iPhone/17.x&diff=prev&oldid=187331
        if info['build'] == '21A340' and set(models).issubset({"iPhone15,4", "iPhone15,5", "iPhone16,1", "iPhone16,2"}):
            assert info['released'] == '2023-09-21'
            info['released'] = "{{n/a}}"

        # Basebands for preinstalled iOS 15;
        # we can't use basebands.yaml because it doesn't support device-specific versions.
        if info['build'] in {'19A341', '19A345'} and all(model.startswith('iPhone14,') for model in models) and info['baseband'] == '?':
            info['baseband'] = '1.00.03'
        if info['build'] in {'19A342'} and set(models).issubset({"iPad14,1", "iPad14,2"}) and info['baseband'] == '?':
            info['baseband'] = '1.00.03'
        if info['build'] in {'21E5209b'} and set(models).issubset({"iPhone12,8"}) and info['baseband'] == '?':
            info['baseband'] = '5.00.00'
        if info['build'] in {'21A5277j'} and set(models).issubset({"iPhone13,4"}) and info['baseband'] == '?':
            info['baseband'] = '4.02.00'

        # Apple Watch Series 9 and Ultra 2 had a second variation with 10.1.1 preinstalled, but don't mark it as such in the wiki
        if info['build'] == '21S71' and set(models).issubset({"Watch7,1", "Watch7,2", "Watch7,3", "Watch7,4", "Watch7,5"}):
            info['_preinstalled'] = False

        for field_name in ('version', 'iosVersion', 'baseband'):
            info[field_name] = SpannedField(info[field_name])
        for bb_prefix in basebands:
            basebands[bb_prefix] = SpannedField(basebands[bb_prefix])

        infos.append(info)

    if len(infos) > 0:
        for field_name in ('version', 'iosVersion', 'baseband'):
            SpannedField.merge_cells([info[field_name] for info in infos])
        # assuming all infos have the same bbfw prefixes
        for bb_prefix in infos[-1]['basebands']:
            SpannedField.merge_cells([info['basebands'][bb_prefix] for info in infos])

    return infos

def wiki_date(date):
    """Format a date into the {{date|yyyy|mm|dd}} wiki template"""
    if not date:
        return '?'
    try:
        y,m,d = date.split('-')
        return '{{date|%04d|%02d|%02d}}' % (int(y), int(m), int(d))
    except ValueError:
        return date

env = Environment(
    loader=FileSystemLoader("templates"),
    autoescape=select_autoescape(),
    trim_blocks=True
)
env.filters['wiki_date'] = wiki_date

buildtrains = yaml.safe_load(open("buildtrains.yaml","r"))
g_basebands = yaml.safe_load(open("basebands.yaml", "r"))

if not os.path.exists("output"):
    os.mkdir("output")

wiki_files = {
    "Firmware_Apple_Vision_1.x":    "vision1",
    "Beta_Firmware_Apple_Vision_1.x":"vision1-beta",
    "Beta_Firmware_Apple_Watch_9.x":"watch9-beta",
    "Beta_Firmware_Apple_Watch_10.x":"watch10-beta",
    "Firmware_Apple_Watch_9.x":     "watch9",
    "Firmware_Apple_Watch_10.x":    "watch10",
    "Firmware_Mac_11.x":            "mac11",
    "Firmware_Mac_12.x":            "mac12",
    "Firmware_Mac_13.x":            "mac13",
    "Firmware_Mac_14.x":            "mac14",
    "Beta_Firmware_Mac_11.x":       "mac11-beta",
    "Beta_Firmware_Mac_12.x":       "mac12-beta",
    "Beta_Firmware_Mac_13.x":       "mac13-beta",
    "Beta_Firmware_Mac_14.x":       "mac14-beta",
    "Firmware_iPhone_15.x":         "iphone15",
    "Firmware_iPhone_16.x":         "iphone16",
    "Firmware_iPhone_17.x":         "iphone17",
    "Firmware_iPad_15.x":           "ipad15",
    "Firmware_iPad_16.x":           "ipad16",
    "Firmware_iPad_17.x":           "ipad17",
    "Firmware_iPad_Air_15.x":       "ipadair15",
    "Firmware_iPad_Air_16.x":       "ipadair16",
    "Firmware_iPad_Air_17.x":       "ipadair17",
    "Firmware_iPad_Pro_15.x":       "ipadpro15",
    "Firmware_iPad_Pro_16.x":       "ipadpro16",
    "Firmware_iPad_Pro_17.x":       "ipadpro17",
    "Firmware_iPad_mini_15.x":      "ipadmini15",
    "Firmware_iPad_mini_16.x":      "ipadmini16",
    "Firmware_iPad_mini_17.x":      "ipadmini17",
    "Firmware_iPod_touch_15.x":     "ipod15",
    "Beta_Firmware_iPod_touch_15.x":"ipod15-beta",
    "Beta_Firmware_iPhone_15.x":    "iphone15-beta",
    "Beta_Firmware_iPhone_16.x":    "iphone16-beta",
    "Beta_Firmware_iPhone_17.x":    "iphone17-beta",
    "Beta_Firmware_iPad_15.x":      "ipad15-beta",
    "Beta_Firmware_iPad_16.x":      "ipad16-beta",
    "Beta_Firmware_iPad_17.x":      "ipad17-beta",
    "Beta_Firmware_iPad_Air_15.x":  "ipadair15-beta",
    "Beta_Firmware_iPad_Air_16.x":  "ipadair16-beta",
    "Beta_Firmware_iPad_Air_17.x":  "ipadair17-beta",
    "Beta_Firmware_iPad_Pro_15.x":  "ipadpro15-beta",
    "Beta_Firmware_iPad_Pro_16.x":  "ipadpro16-beta",
    "Beta_Firmware_iPad_Pro_17.x":  "ipadpro17-beta",
    "Beta_Firmware_iPad_mini_15.x": "ipadmini15-beta",
    "Beta_Firmware_iPad_mini_16.x": "ipadmini16-beta",
    "Beta_Firmware_iPad_mini_17.x": "ipadmini17-beta",
    "Beta_Firmware_HomePod_15.x":   "homepod15-beta",
    "Beta_Firmware_HomePod_16.x":   "homepod16-beta",
    "Beta_Firmware_HomePod_17.x":   "homepod17-beta",
    "Beta_Firmware_Apple_TV_15.x":  "appletv15-beta",
    "Beta_Firmware_Apple_TV_16.x":  "appletv16-beta",
    "Beta_Firmware_Apple_TV_17.x":  "appletv17-beta",
    "Firmware_HomePod_15.x":        "homepod15",
    "Firmware_HomePod_16.x":        "homepod16",
    "Firmware_HomePod_17.x":        "homepod17",
    "Firmware_Apple_TV_15.x":       "appletv15",
    "Firmware_Apple_TV_16.x":       "appletv16",
    "Firmware_Apple_TV_17.x":       "appletv17",
}

# only generate pages given as CLI arguments, if any
wanted_pages = None
if len(sys.argv) > 1:
    wanted_pages = sys.argv[1:]

for wiki_file, template_file in sorted(wiki_files.items(), key=lambda x: x[0]):
    if wanted_pages is not None and wiki_file not in wanted_pages:
        continue

    print(f"Generating {wiki_file}...", file=sys.stderr)
    with open(f"output/{wiki_file}.wiki", "w") as f:
        template = env.get_template(template_file+".jinja")
        f.write(f"<!-- This page is auto-generated from AppleDB data: https://github.com/littlebyteorg/appledb -->\n")
        f.write(f"<!-- using this template: https://gitlab.com/nicolas17/autoiphonewiki/-/blob/master/templates/{template_file}.jinja -->\n")
        f.write("{{nobots}}\n")
        f.write(template.render(get_firmwares=get_firmwares))
