<!-- This page is auto-generated from AppleDB data: https://github.com/littlebyteorg/appledb -->
<!-- using this template: https://gitlab.com/nicolas17/autoiphonewiki/-/blob/master/templates/appletv17-beta.jinja -->
{{nobots}}
== [[Apple TV HD]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Release Date
! Download URL
! File Size
|-
| 17.0 beta
| 21J5273q
| [[Keys:StarlightSeed 21J5273q (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|06|05}}
| [https://developer.apple.com/services-account/download?path=/WWDC_2023/tvOS_17_beta_Restore_Images/AppleTV53_17.0_21J5273q_Restore.ipsw AppleTV53_17.0_21J5273q_Restore.ipsw]
| 4,144,162,456
|-
| 17.0 beta 2
| 21J5293g
| [[Keys:StarlightSeed 21J5293g (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|06|21}}
| [https://updates.cdn-apple.com/2023SummerSeed/fullrestores/042-04740/4BDDB9DB-B7C3-4906-A606-6C6298FB28C2/AppleTV5,3_17.0_21J5293g_Restore.ipsw AppleTV5,3_17.0_21J5293g_Restore.ipsw]
| 4,148,046,836
|-
| rowspan="2" | 17.0 beta 3
| 21J5303f
| [[Keys:StarlightSeed 21J5303f (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|07|05}}
| [https://updates.cdn-apple.com/2023SummerSeed/fullrestores/042-06658/31149541-0AA3-424E-9D0E-B66710E898CF/AppleTV5,3_17.0_21J5303f_Restore.ipsw AppleTV5,3_17.0_21J5303f_Restore.ipsw]
| 4,154,834,961
|-
| 21J5303h
| [[Keys:StarlightSeed 21J5303h (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|07|11}}
| [https://updates.cdn-apple.com/2023SummerSeed/fullrestores/042-12558/FDAA2F9D-E13E-4CA6-A136-9DE2F6388C00/AppleTV5,3_17.0_21J5303h_Restore.ipsw AppleTV5,3_17.0_21J5303h_Restore.ipsw]
| 4,155,061,942
|-
| 17.0 beta 4
| 21J5318f
| [[Keys:StarlightSeed 21J5318f (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|07|25}}
| [https://updates.cdn-apple.com/2023SummerSeed/fullrestores/042-16491/C10543E0-CCCC-4A66-8E54-3379D4832A8F/AppleTV5,3_17.0_21J5318f_Restore.ipsw AppleTV5,3_17.0_21J5318f_Restore.ipsw]
| 4,169,237,664
|-
| 17.0 beta 5
| 21J5330e
| [[Keys:StarlightSeed 21J5330e (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|08|08}}
| [https://updates.cdn-apple.com/2023SummerSeed/fullrestores/042-23644/DCB03D92-BFE8-4A42-8B48-4760A4B5F517/AppleTV5,3_17.0_21J5330e_Restore.ipsw AppleTV5,3_17.0_21J5330e_Restore.ipsw]
| 4,186,921,795
|-
| 17.0 beta 6
| 21J5339b
| [[Keys:StarlightSeed 21J5339b (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|08|15}}
| [https://updates.cdn-apple.com/2023SummerSeed/fullrestores/042-28845/49FF0E4A-40D3-471D-9C34-952E3D084BA4/AppleTV5,3_17.0_21J5339b_Restore.ipsw AppleTV5,3_17.0_21J5339b_Restore.ipsw]
| 4,187,402,659
|-
| 17.0 beta 7
| 21J5347a
| [[Keys:StarlightSeed 21J5347a (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|08|22}}
| [https://updates.cdn-apple.com/2023SummerSeed/fullrestores/042-37812/838F6842-D48A-4B7E-A1AD-19DF875927E8/AppleTV5,3_17.0_21J5347a_Restore.ipsw AppleTV5,3_17.0_21J5347a_Restore.ipsw]
| 4,190,430,461
|-
| 17.0 beta 8
| 21J5353a
| [[Keys:StarlightSeed 21J5353a (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|08|29}}
| [https://updates.cdn-apple.com/2023SummerSeed/fullrestores/042-41361/DC305FDD-AE06-40DB-947D-58FEE2DC4C6A/AppleTV5,3_17.0_21J5353a_Restore.ipsw AppleTV5,3_17.0_21J5353a_Restore.ipsw]
| 4,190,241,674
|-
| 17.0 beta 9
| 21J5354a
| [[Keys:StarlightSeed 21J5354a (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|09|05}}
| [https://updates.cdn-apple.com/2023SummerSeed/fullrestores/042-43329/434119D3-35EB-4A2F-8567-13D6E6A6064B/AppleTV5,3_17.0_21J5354a_Restore.ipsw AppleTV5,3_17.0_21J5354a_Restore.ipsw]
| 4,189,407,446
|-
| 17.0 [[Release Candidate|RC]]
| 21J354
| [[Keys:Starlight 21J354 (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|09|12}}
| [https://updates.cdn-apple.com/2023FallFCS/fullrestores/002-77883/6A48B51B-36BE-4CF6-8B7C-35667D3F3FC0/AppleTV5,3_17.0_21J354_Restore.ipsw AppleTV5,3_17.0_21J354_Restore.ipsw]
| 4,197,799,716
|-
| 17.1 beta
| 21K5043e
| [[Keys:StarlightBSeed 21K5043e (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|09|27}}
| [https://updates.cdn-apple.com/2023FallSeed/fullrestores/042-41621/9F4361F7-469E-46B5-AA24-6AE2CC462182/AppleTV5,3_17.1_21K5043e_Restore.ipsw AppleTV5,3_17.1_21K5043e_Restore.ipsw]
| 4,204,001,564
|-
| 17.1 beta 2
| 21K5054e
| [[Keys:StarlightBSeed 21K5054e (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|10|03}}
| [https://updates.cdn-apple.com/2023FallSeed/fullrestores/042-66879/EB344B44-8A73-4D8F-830E-2614747B252F/AppleTV5,3_17.1_21K5054e_Restore.ipsw AppleTV5,3_17.1_21K5054e_Restore.ipsw]
| 4,205,987,215
|-
| 17.1 beta 3
| 21K5064b
| [[Keys:StarlightBSeed 21K5064b (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|10|11}}
| [https://updates.cdn-apple.com/2023FallSeed/fullrestores/042-81480/36F7BFB7-FF85-4714-BFFB-6B2A1E6935F8/AppleTV5,3_17.1_21K5064b_Restore.ipsw AppleTV5,3_17.1_21K5064b_Restore.ipsw]
| 4,205,330,279
|-
| 17.1 [[Release Candidate|RC]]
| 21K69
| [[Keys:StarlightB 21K69 (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|10|17}}
| [https://updates.cdn-apple.com/2023FallFCS/fullrestores/042-83727/CE095362-ACC4-4A6C-9A8A-B7C91E862864/AppleTV5,3_17.1_21K69_Restore.ipsw AppleTV5,3_17.1_21K69_Restore.ipsw]
| 4,209,550,548
|-
| 17.2 beta
| 21K5330g
| [[Keys:StarlightCSeed 21K5330g (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|10|26}}
| [https://updates.cdn-apple.com/2023FallSeed/fullrestores/042-87897/0D96A3D5-6E43-411F-A189-30F306CE5B83/AppleTV5,3_17.2_21K5330g_Restore.ipsw AppleTV5,3_17.2_21K5330g_Restore.ipsw]
| 4,262,348,895
|-
| 17.2 beta 2
| 21K5341f
| [[Keys:StarlightCSeed 21K5341f (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|11|09}}
| [https://updates.cdn-apple.com/2023FallSeed/fullrestores/042-89967/8966B5D1-4A6B-49F6-A4EE-ED2B5640F1F7/AppleTV5,3_17.2_21K5341f_Restore.ipsw AppleTV5,3_17.2_21K5341f_Restore.ipsw]
| 4,265,368,710
|-
| 17.2 beta 3
| 21K5348f
| [[Keys:StarlightCSeed 21K5348f (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|11|14}}
| [https://updates.cdn-apple.com/2023FallSeed/fullrestores/042-99906/3ECD72F4-4283-49CA-A45E-C9227D8D96B9/AppleTV5,3_17.2_21K5348f_Restore.ipsw AppleTV5,3_17.2_21K5348f_Restore.ipsw]
| 4,284,565,749
|-
| 17.2 beta 4
| 21K5356c
| [[Keys:StarlightCSeed 21K5356c (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|11|28}}
| [https://updates.cdn-apple.com/2023FallSeed/fullrestores/052-07889/00E3DD4E-3A0E-4AA0-B3AB-96B0D98FCE72/AppleTV5,3_17.2_21K5356c_Restore.ipsw AppleTV5,3_17.2_21K5356c_Restore.ipsw]
| 4,297,448,543
|-
| 17.2 [[Release Candidate|RC]]
| 21K364
| [[Keys:StarlightC 21K364 (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|12|05}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/042-37208/8D6BF8C6-98AD-414D-8B8C-C856036D6E47/AppleTV5,3_17.2_21K364_Restore.ipsw AppleTV5,3_17.2_21K364_Restore.ipsw]
| 4,299,185,735
|-
| 17.2 [[Release Candidate|RC]] 2
| 21K365
| [[Keys:StarlightC 21K365 (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|12|08}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/052-15408/9B221821-ED48-43CA-8663-5BB2AAA13B07/AppleTV5,3_17.2_21K365_Restore.ipsw AppleTV5,3_17.2_21K365_Restore.ipsw]
| 4,298,860,542
|-
| 17.3 beta
| 21K5625e
| [[Keys:StarlightDSeed 21K5625e (AppleTV5,3)|AppleTV5,3]]
| {{date|2023|12|12}}
| [https://updates.cdn-apple.com/2024WinterSeed/fullrestores/052-03724/0C7C84AB-EF0B-402A-A922-ED8B61252EC0/AppleTV5,3_17.3_21K5625e_Restore.ipsw AppleTV5,3_17.3_21K5625e_Restore.ipsw]
| 4,302,020,521
|-
| 17.3 beta 2
| 21K5635c
| [[Keys:StarlightDSeed 21K5635c (AppleTV5,3)|AppleTV5,3]]
| {{date|2024|01|03}}
| [https://updates.cdn-apple.com/2024WinterSeed/fullrestores/052-26846/02928CC2-19BA-4D81-B6DA-5D77749C0BD5/AppleTV5,3_17.3_21K5635c_Restore.ipsw AppleTV5,3_17.3_21K5635c_Restore.ipsw]
| 4,302,245,350
|-
| 17.3 beta 3
| 21K5643b
| [[Keys:StarlightDSeed 21K5643b (AppleTV5,3)|AppleTV5,3]]
| {{date|2024|01|09}}
| [https://updates.cdn-apple.com/2024WinterSeed/fullrestores/052-32827/BA1F8AE4-77B9-48E5-A49A-B09632691C0D/AppleTV5,3_17.3_21K5643b_Restore.ipsw AppleTV5,3_17.3_21K5643b_Restore.ipsw]
| 4,299,559,822
|-
| 17.3 [[Release Candidate|RC]]
| 21K646
| [[Keys:StarlightD 21K646 (AppleTV5,3)|AppleTV5,3]]
| {{date|2024|01|17}}
| [https://updates.cdn-apple.com/2023WinterFCS/fullrestores/042-76229/5FCF1690-C794-405A-ADE7-E81921A46028/AppleTV5,3_17.3_21K646_Restore.ipsw AppleTV5,3_17.3_21K646_Restore.ipsw]
| 4,301,370,714
|-
| 17.4 beta
| 21L5195h
| [[Keys:StarlightESeed 21L5195h (AppleTV5,3)|AppleTV5,3]]
| {{date|2024|01|25}}
| [https://updates.cdn-apple.com/2024WinterSeed/fullrestores/052-11655/1B83C891-35A2-4299-9F42-760C0980C135/AppleTV5,3_17.4_21L5195h_Restore.ipsw AppleTV5,3_17.4_21L5195h_Restore.ipsw]
| 4,372,561,085
|-
| 17.4 beta 2
| 21L5206f
| [[Keys:StarlightESeed 21L5206f (AppleTV5,3)|AppleTV5,3]]
| {{date|2024|02|06}}
| [https://updates.cdn-apple.com/2024WinterSeed/fullrestores/052-45288/7752195F-1C8C-4FDE-BDD5-81695B969346/AppleTV5,3_17.4_21L5206f_Restore.ipsw AppleTV5,3_17.4_21L5206f_Restore.ipsw]
| 4,376,689,400
|-
| 17.4 beta 3
| 21L5212d
| [[Keys:StarlightESeed 21L5212d (AppleTV5,3)|AppleTV5,3]]
| {{date|2024|02|13}}
| [https://updates.cdn-apple.com/2024WinterSeed/fullrestores/052-51890/7E52089B-D747-4DC6-8A9D-D495FA72D2BE/AppleTV5,3_17.4_21L5212d_Restore.ipsw AppleTV5,3_17.4_21L5212d_Restore.ipsw]
| 4,373,728,485
|-
| 17.4 beta 4
| 21L5222a
| [[Keys:StarlightESeed 21L5222a (AppleTV5,3)|AppleTV5,3]]
| {{date|2024|02|20}}
| [https://updates.cdn-apple.com/2024WinterSeed/fullrestores/052-55950/7E8B0799-240C-40D5-99B3-DEA4AD6854DF/AppleTV5,3_17.4_21L5222a_Restore.ipsw AppleTV5,3_17.4_21L5222a_Restore.ipsw]
| 4,378,807,236
|-
| 17.4 beta 5
| 21L5225a
| [[Keys:StarlightESeed 21L5225a (AppleTV5,3)|AppleTV5,3]]
| {{date|2024|02|27}}
| [https://updates.cdn-apple.com/2024WinterSeed/fullrestores/052-58524/6839EEC1-9031-4887-B761-A624BA8A6C2A/AppleTV5,3_17.4_21L5225a_Restore.ipsw AppleTV5,3_17.4_21L5225a_Restore.ipsw]
| 4,379,883,141
|-
| 17.4 [[Release Candidate|RC]]
| 21L227
| [[Keys:StarlightE 21L227 (AppleTV5,3)|AppleTV5,3]]
| {{date|2024|03|04}}
| [https://updates.cdn-apple.com/2024WinterFCS/fullrestores/032-53412/3254A4D8-C457-4636-9F99-86650458C35A/AppleTV5,3_17.4_21L227_Restore.ipsw AppleTV5,3_17.4_21L227_Restore.ipsw]
| 4,391,644,267
|}

== [[Apple TV 4K]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Release Date
|-
| 17.0 beta
| 21J5273q
| [[Keys:StarlightSeed 21J5273q (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|06|05}}
|-
| 17.0 beta 2
| 21J5293g
| [[Keys:StarlightSeed 21J5293g (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|06|21}}
|-
| rowspan="2" | 17.0 beta 3
| 21J5303f
| [[Keys:StarlightSeed 21J5303f (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|07|05}}
|-
| 21J5303h
| [[Keys:StarlightSeed 21J5303h (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|07|11}}
|-
| 17.0 beta 4
| 21J5318f
| [[Keys:StarlightSeed 21J5318f (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|07|25}}
|-
| 17.0 beta 5
| 21J5330e
| [[Keys:StarlightSeed 21J5330e (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|08|08}}
|-
| 17.0 beta 6
| 21J5339b
| [[Keys:StarlightSeed 21J5339b (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|08|15}}
|-
| 17.0 beta 7
| 21J5347a
| [[Keys:StarlightSeed 21J5347a (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|08|22}}
|-
| 17.0 beta 8
| 21J5353a
| [[Keys:StarlightSeed 21J5353a (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|08|29}}
|-
| 17.0 beta 9
| 21J5354a
| [[Keys:StarlightSeed 21J5354a (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|09|05}}
|-
| 17.0 [[Release Candidate|RC]]
| 21J354
| [[Keys:Starlight 21J354 (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|09|12}}
|-
| 17.1 beta
| 21K5043e
| [[Keys:StarlightBSeed 21K5043e (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|09|27}}
|-
| 17.1 beta 2
| 21K5054e
| [[Keys:StarlightBSeed 21K5054e (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|10|03}}
|-
| 17.1 beta 3
| 21K5064b
| [[Keys:StarlightBSeed 21K5064b (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|10|11}}
|-
| 17.1 [[Release Candidate|RC]]
| 21K69
| [[Keys:StarlightB 21K69 (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|10|17}}
|-
| 17.2 beta
| 21K5330g
| [[Keys:StarlightCSeed 21K5330g (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|10|26}}
|-
| 17.2 beta 2
| 21K5341f
| [[Keys:StarlightCSeed 21K5341f (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|11|09}}
|-
| 17.2 beta 3
| 21K5348f
| [[Keys:StarlightCSeed 21K5348f (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|11|14}}
|-
| 17.2 beta 4
| 21K5356c
| [[Keys:StarlightCSeed 21K5356c (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|11|28}}
|-
| 17.2 [[Release Candidate|RC]]
| 21K364
| [[Keys:StarlightC 21K364 (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|12|05}}
|-
| 17.2 [[Release Candidate|RC]] 2
| 21K365
| [[Keys:StarlightC 21K365 (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|12|08}}
|-
| 17.3 beta
| 21K5625e
| [[Keys:StarlightDSeed 21K5625e (AppleTV6,2)|AppleTV6,2]]
| {{date|2023|12|12}}
|-
| 17.3 beta 2
| 21K5635c
| [[Keys:StarlightDSeed 21K5635c (AppleTV6,2)|AppleTV6,2]]
| {{date|2024|01|03}}
|-
| 17.3 beta 3
| 21K5643b
| [[Keys:StarlightDSeed 21K5643b (AppleTV6,2)|AppleTV6,2]]
| {{date|2024|01|09}}
|-
| 17.3 [[Release Candidate|RC]]
| 21K646
| [[Keys:StarlightD 21K646 (AppleTV6,2)|AppleTV6,2]]
| {{date|2024|01|17}}
|-
| 17.4 beta
| 21L5195h
| [[Keys:StarlightESeed 21L5195h (AppleTV6,2)|AppleTV6,2]]
| {{date|2024|01|25}}
|-
| 17.4 beta 2
| 21L5206f
| [[Keys:StarlightESeed 21L5206f (AppleTV6,2)|AppleTV6,2]]
| {{date|2024|02|06}}
|-
| 17.4 beta 3
| 21L5212d
| [[Keys:StarlightESeed 21L5212d (AppleTV6,2)|AppleTV6,2]]
| {{date|2024|02|13}}
|-
| 17.4 beta 4
| 21L5222a
| [[Keys:StarlightESeed 21L5222a (AppleTV6,2)|AppleTV6,2]]
| {{date|2024|02|20}}
|-
| 17.4 beta 5
| 21L5225a
| [[Keys:StarlightESeed 21L5225a (AppleTV6,2)|AppleTV6,2]]
| {{date|2024|02|27}}
|-
| 17.4 [[Release Candidate|RC]]
| 21L227
| [[Keys:StarlightE 21L227 (AppleTV6,2)|AppleTV6,2]]
| {{date|2024|03|04}}
|}

== [[Apple TV 4K (2nd generation)]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Release Date
|-
| 17.0 beta
| 21J5273q
| [[Keys:StarlightSeed 21J5273q (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|06|05}}
|-
| 17.0 beta 2
| 21J5293g
| [[Keys:StarlightSeed 21J5293g (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|06|21}}
|-
| rowspan="2" | 17.0 beta 3
| 21J5303f
| [[Keys:StarlightSeed 21J5303f (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|07|05}}
|-
| 21J5303h
| [[Keys:StarlightSeed 21J5303h (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|07|11}}
|-
| 17.0 beta 4
| 21J5318f
| [[Keys:StarlightSeed 21J5318f (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|07|25}}
|-
| 17.0 beta 5
| 21J5330e
| [[Keys:StarlightSeed 21J5330e (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|08|08}}
|-
| 17.0 beta 6
| 21J5339b
| [[Keys:StarlightSeed 21J5339b (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|08|15}}
|-
| 17.0 beta 7
| 21J5347a
| [[Keys:StarlightSeed 21J5347a (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|08|22}}
|-
| 17.0 beta 8
| 21J5353a
| [[Keys:StarlightSeed 21J5353a (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|08|29}}
|-
| 17.0 beta 9
| 21J5354a
| [[Keys:StarlightSeed 21J5354a (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|09|05}}
|-
| 17.0 [[Release Candidate|RC]]
| 21J354
| [[Keys:Starlight 21J354 (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|09|12}}
|-
| 17.1 beta
| 21K5043e
| [[Keys:StarlightBSeed 21K5043e (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|09|27}}
|-
| 17.1 beta 2
| 21K5054e
| [[Keys:StarlightBSeed 21K5054e (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|10|03}}
|-
| 17.1 beta 3
| 21K5064b
| [[Keys:StarlightBSeed 21K5064b (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|10|11}}
|-
| 17.1 [[Release Candidate|RC]]
| 21K69
| [[Keys:StarlightB 21K69 (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|10|17}}
|-
| 17.2 beta
| 21K5330g
| [[Keys:StarlightCSeed 21K5330g (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|10|26}}
|-
| 17.2 beta 2
| 21K5341f
| [[Keys:StarlightCSeed 21K5341f (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|11|09}}
|-
| 17.2 beta 3
| 21K5348f
| [[Keys:StarlightCSeed 21K5348f (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|11|14}}
|-
| 17.2 beta 4
| 21K5356c
| [[Keys:StarlightCSeed 21K5356c (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|11|28}}
|-
| 17.2 [[Release Candidate|RC]]
| 21K364
| [[Keys:StarlightC 21K364 (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|12|05}}
|-
| 17.2 [[Release Candidate|RC]] 2
| 21K365
| [[Keys:StarlightC 21K365 (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|12|08}}
|-
| 17.3 beta
| 21K5625e
| [[Keys:StarlightDSeed 21K5625e (AppleTV11,1)|AppleTV11,1]]
| {{date|2023|12|12}}
|-
| 17.3 beta 2
| 21K5635c
| [[Keys:StarlightDSeed 21K5635c (AppleTV11,1)|AppleTV11,1]]
| {{date|2024|01|03}}
|-
| 17.3 beta 3
| 21K5643b
| [[Keys:StarlightDSeed 21K5643b (AppleTV11,1)|AppleTV11,1]]
| {{date|2024|01|09}}
|-
| 17.3 [[Release Candidate|RC]]
| 21K646
| [[Keys:StarlightD 21K646 (AppleTV11,1)|AppleTV11,1]]
| {{date|2024|01|17}}
|-
| 17.4 beta
| 21L5195h
| [[Keys:StarlightESeed 21L5195h (AppleTV11,1)|AppleTV11,1]]
| {{date|2024|01|25}}
|-
| 17.4 beta 2
| 21L5206f
| [[Keys:StarlightESeed 21L5206f (AppleTV11,1)|AppleTV11,1]]
| {{date|2024|02|06}}
|-
| 17.4 beta 3
| 21L5212d
| [[Keys:StarlightESeed 21L5212d (AppleTV11,1)|AppleTV11,1]]
| {{date|2024|02|13}}
|-
| 17.4 beta 4
| 21L5222a
| [[Keys:StarlightESeed 21L5222a (AppleTV11,1)|AppleTV11,1]]
| {{date|2024|02|20}}
|-
| 17.4 beta 5
| 21L5225a
| [[Keys:StarlightESeed 21L5225a (AppleTV11,1)|AppleTV11,1]]
| {{date|2024|02|27}}
|-
| 17.4 [[Release Candidate|RC]]
| 21L227
| [[Keys:StarlightE 21L227 (AppleTV11,1)|AppleTV11,1]]
| {{date|2024|03|04}}
|}

== [[Apple TV 4K (3rd generation)]] ==
{| class="wikitable"
|-
! Version
! Build
! Keys
! Release Date
|-
| 17.0 beta
| 21J5273q
| [[Keys:StarlightSeed 21J5273q (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|06|05}}
|-
| 17.0 beta 2
| 21J5293g
| [[Keys:StarlightSeed 21J5293g (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|06|21}}
|-
| rowspan="2" | 17.0 beta 3
| 21J5303f
| [[Keys:StarlightSeed 21J5303f (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|07|05}}
|-
| 21J5303h
| [[Keys:StarlightSeed 21J5303h (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|07|11}}
|-
| 17.0 beta 4
| 21J5318f
| [[Keys:StarlightSeed 21J5318f (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|07|25}}
|-
| 17.0 beta 5
| 21J5330e
| [[Keys:StarlightSeed 21J5330e (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|08|08}}
|-
| 17.0 beta 6
| 21J5339b
| [[Keys:StarlightSeed 21J5339b (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|08|15}}
|-
| 17.0 beta 7
| 21J5347a
| [[Keys:StarlightSeed 21J5347a (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|08|22}}
|-
| 17.0 beta 8
| 21J5353a
| [[Keys:StarlightSeed 21J5353a (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|08|29}}
|-
| 17.0 beta 9
| 21J5354a
| [[Keys:StarlightSeed 21J5354a (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|09|05}}
|-
| 17.0 [[Release Candidate|RC]]
| 21J354
| [[Keys:Starlight 21J354 (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|09|12}}
|-
| 17.1 beta
| 21K5043e
| [[Keys:StarlightBSeed 21K5043e (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|09|27}}
|-
| 17.1 beta 2
| 21K5054e
| [[Keys:StarlightBSeed 21K5054e (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|10|03}}
|-
| 17.1 beta 3
| 21K5064b
| [[Keys:StarlightBSeed 21K5064b (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|10|11}}
|-
| 17.1 [[Release Candidate|RC]]
| 21K69
| [[Keys:StarlightB 21K69 (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|10|17}}
|-
| 17.2 beta
| 21K5330g
| [[Keys:StarlightCSeed 21K5330g (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|10|26}}
|-
| 17.2 beta 2
| 21K5341f
| [[Keys:StarlightCSeed 21K5341f (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|11|09}}
|-
| 17.2 beta 3
| 21K5348f
| [[Keys:StarlightCSeed 21K5348f (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|11|14}}
|-
| 17.2 beta 4
| 21K5356c
| [[Keys:StarlightCSeed 21K5356c (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|11|28}}
|-
| 17.2 [[Release Candidate|RC]]
| 21K364
| [[Keys:StarlightC 21K364 (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|12|05}}
|-
| 17.2 [[Release Candidate|RC]] 2
| 21K365
| [[Keys:StarlightC 21K365 (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|12|08}}
|-
| 17.3 beta
| 21K5625e
| [[Keys:StarlightDSeed 21K5625e (AppleTV14,1)|AppleTV14,1]]
| {{date|2023|12|12}}
|-
| 17.3 beta 2
| 21K5635c
| [[Keys:StarlightDSeed 21K5635c (AppleTV14,1)|AppleTV14,1]]
| {{date|2024|01|03}}
|-
| 17.3 beta 3
| 21K5643b
| [[Keys:StarlightDSeed 21K5643b (AppleTV14,1)|AppleTV14,1]]
| {{date|2024|01|09}}
|-
| 17.3 [[Release Candidate|RC]]
| 21K646
| [[Keys:StarlightD 21K646 (AppleTV14,1)|AppleTV14,1]]
| {{date|2024|01|17}}
|-
| 17.4 beta
| 21L5195h
| [[Keys:StarlightESeed 21L5195h (AppleTV14,1)|AppleTV14,1]]
| {{date|2024|01|25}}
|-
| 17.4 beta 2
| 21L5206f
| [[Keys:StarlightESeed 21L5206f (AppleTV14,1)|AppleTV14,1]]
| {{date|2024|02|06}}
|-
| 17.4 beta 3
| 21L5212d
| [[Keys:StarlightESeed 21L5212d (AppleTV14,1)|AppleTV14,1]]
| {{date|2024|02|13}}
|-
| 17.4 beta 4
| 21L5222a
| [[Keys:StarlightESeed 21L5222a (AppleTV14,1)|AppleTV14,1]]
| {{date|2024|02|20}}
|-
| 17.4 beta 5
| 21L5225a
| [[Keys:StarlightESeed 21L5225a (AppleTV14,1)|AppleTV14,1]]
| {{date|2024|02|27}}
|-
| 17.4 [[Release Candidate|RC]]
| 21L227
| [[Keys:StarlightE 21L227 (AppleTV14,1)|AppleTV14,1]]
| {{date|2024|03|04}}
|}

[[Category:Firmware]]